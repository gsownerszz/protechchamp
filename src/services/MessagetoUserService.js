import { Notify } from 'quasar'

export default {
  showAlertInfo (userMessage, pos = 'right', color = 'primary') {
    Notify.create({
      color: color,
      position: pos,
      message: userMessage,
      icon: 'info_outline'
    })
  },
  showAlertError (userMessage, pos = 'right', color = 'negative') {
    Notify.create({
      color: color,
      position: pos,
      message: userMessage,
      icon: 'error_outline',
      progress: true,
      progressClass: { 'background-color': 'black' },
      attrs: { role: 'alertdialog' }
    })
  },
  log (error) {
    console.log(error)
    this.$q.notify({
      message: 'Jim pinged you',
      color: 'purple',
      avatar: 'https://cdn.quasar.dev/img/boy-avatar.png'
    })
  }
}
