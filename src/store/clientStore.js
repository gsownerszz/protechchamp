/* eslint-disable no-empty-pattern */
/* eslint-disable no-unused-vars */
import { firebaseAuth, firebaseDB } from 'boot/firebase'
// import msgToUser from '../services/MessagetoUserService'
import { Loading, QSpinnerPie } from 'quasar'

const state = {
  count: '',

  clients: {},
  client: {
    name: 'gs',
    email: 'gg@techchamp.com'
  }
}

const mutations = {
  setClients (state, payload) {
    state.clients = payload
  },
  clearClients (state) {
    state.clients = ''
  }
}

const actions = {
  // actions can be runned everywhere
  syncClients ({ commit }) {
    Loading.show(
      {
        spinner: QSpinnerPie,
        spinnerColor: 'primary',
        message: 'Logging you in <br> <span class="text-primary">please wait</span>',
        spinnerSize: 160
      }
    )
    if (firebaseAuth.currentUser) {
      const userID = firebaseAuth.currentUser.uid
      firebaseDB.ref('clients/').on('value', snapshot => {
        const clients = snapshot.val()
        commit('setClients', {
          clients
        })
      })
    }
    Loading.hide()
  },
  updateClient ({ commit }, payload) {},
  removeClient ({ }, payload) {
    if (firebaseAuth.currentUser) {
      const userID = firebaseAuth.currentUser.uid
      console.log(payload)
      firebaseDB.ref('clients/' + payload).remove()
    }
  },
  addClient ({ commit }, payload) {
    var gs = firebaseDB.ref('clients/client' + payload.id).set({
      name: payload.name,
      email: payload.email,
      gender: payload.gender,
      Tnmr: payload.tnmr
    })
    console.log(gs)
  }

}

const getters = {

}

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}
