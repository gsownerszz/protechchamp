/* eslint-disable no-empty-pattern */
/* eslint-disable no-unused-vars */
import { firebaseAuth, firebaseDB } from 'boot/firebase'
import msgToUser from '../services/MessagetoUserService'
import { Loading, QSpinnerPie } from 'quasar'

const state = {
  count: 'qsgd',
  user: {},
  clients: {},
  settings: {
    mode: 'dark',
    url: 'short',
    sound: 'off',
    location: 'on'
  }

}

const mutations = {
  setLoggedInUser (state, payload) {
    state.user = payload
  },
  resetUser (state) {
    state.user = ''
  }
}

const actions = {
  // actions can be runned everywhere
  logInUser ({}, payload) {
    Loading.show(
      {
        spinner: QSpinnerPie,
        spinnerColor: 'primary',
        message: 'Logging you in <br> <span class="text-primary">please wait</span>',
        spinnerSize: 160
      }
    )
    // console.log(firebaseAuth.currentUser) === undefined (user is niet ingelogd hier)
    firebaseAuth.signInWithEmailAndPassword(payload.login, payload.password).then(() => {
      // log in is correct
      Loading.hide()
    }).catch(error => {
      setTimeout(() => {
        msgToUser.showAlertError(error.code)
        Loading.hide()
      }, 2000)
    })
  },
  logOut ({ commit }) {
    firebaseAuth.signOut().then(() => {
      // log out is correct
      msgToUser.showAlertInfo('logged out correctly')
      commit('resetUser')
      this.$router.push('/')
    }).catch(error => {
      msgToUser.showAlertError("cnn't log you out pleas contact your IT department (errorcode :" + error.code + ')')
    })
  },
  // run this at "beforeMount" of the project (see LoginPage.vue)
  handeleAuthStateChange ({ commit }) {
    firebaseAuth.onAuthStateChanged(user => {
      // user is logged in
      if (user && firebaseAuth.currentUser === user) {
        const userID = firebaseAuth.currentUser.uid
        firebaseDB.ref('Logs/').on('value', snapshot => {
          const LoggedInUserInfo = snapshot.val()
          commit('setLoggedInUser', {
            LoggedInUserInfo
          })
          if (userID === Object.keys(LoggedInUserInfo)[0]) {
            // neem info van user object kan ook Object.keys(LoggedInUserInfo)[0]
            this.$router.push('/' + Object.values(LoggedInUserInfo)[0].Name)
          } else {
            // msgToUser.showAlertError("cnn't log you out pleas contact your IT department (errorcode :" )

            this.logOut()
          }
        })
      } else {
        commit('resetUser')
      }
    })
  },
  // zie deze functie wordt eerste keer gerupen bij het starten LoginPage.vue
  updateUsers ({ commit }) {
    if (firebaseAuth.currentUser) {
      firebaseDB.ref('Logs/').on('value', snapshot => {
        const userdetaild = snapshot.val()
        console.log(userdetaild)
        commit('setLoggedInUser', {
          userdetaild
        })
      })
    }
  }
}

const getters = {

}

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}
