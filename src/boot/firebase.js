// Firebase App (the core Firebase SDK) is always required and
// must be listed before other Firebase SDKs
import firebase from 'firebase/app'

// Add the Firebase services that you want to use
import 'firebase/auth'
import 'firebase/database'

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
  var firebaseConfig = {
    apiKey: "AIzaSyD1eWPi08uyTKUZhd5E7WsFpDT0-OdLPxI",
    authDomain: "clsrv-69443.firebaseapp.com",
    projectId: "clsrv-69443",
    storageBucket: "clsrv-69443.appspot.com",
    messagingSenderId: "98624999971",
    appId: "1:98624999971:web:a2146d4f874f5639441796",
    databaseURL: "https://clsrv-69443-default-rtdb.europe-west1.firebasedatabase.app/",
    measurementId: "G-N8BP9G6936"

  };


// Initialize Firebase
const firebaseApp = firebase.initializeApp(firebaseConfig)
const firebaseAuth = firebaseApp.auth()
const firebaseDB = firebaseApp.database()
// eslint-disable-next-line no-unused-vars

export { firebaseAuth, firebaseDB }
