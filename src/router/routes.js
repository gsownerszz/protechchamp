
const routes = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      {
        path: '',
        component: () => import('pages/LoginPage.vue'),
        props: {
          default: true,
          // function mode, more about it below
          sidebar: route => ({ search: route.query.q })
        }
      }
    ]
  },
  {
    path: '/',
    component: () => import('layouts/LogedInLayout.vue'),
    children: [
      { path: '/:uname', component: () => import('pages/Index.vue') },
      { path: '/:uname/clients', component: () => import('pages/ClientsPage.vue') }
    ]
  },
  // Always leave this as last one,
  // but you can also remove it
  {
    path: '*',
    component: () => import('pages/Error404.vue')
  }
]

export default routes
